import java.io.File;
import java.io.FileNotFoundException;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Scanner;

public class MapDemoLecture {
	
	public static void main(String args[]) {
		// two Collection objects for us to use
		TreeSet<String> allWords = new TreeSet<String>();
		TreeMap<String,String> addressMap = new TreeMap<String,String>();
		
		Scanner fileScanner = getScanner("name-address.txt");
		while ( fileScanner.hasNext() ) {
			// read lines two-at-a-time from a file
			String name = fileScanner.nextLine().trim();
			String address = fileScanner.nextLine().trim();
			System.out.println(name + ":" + address);
						
			// Task 1:  store each person's name in the set

			
			// Task 2:  store each word in the address into the same set

		
			// Task 3: store each (name,address) pair into the map

		}

		System.out.println("** allWords contains:\n" + allWords + "\n");
		System.out.println("** addressMap contains:\n" + addressMap + "\n");
		
		// Task 4: read key and look it up in map (add code in loop body)
		System.out.println("\n** Let's lookup names in addressMap.");
		Scanner keyboard = new Scanner(System.in);
		System.out.print("* Enter name (or exit to quit): ");
		String key = keyboard.next();
		while ( ! key.equals("exit") ) {
			// how do we retrieve the address from the map? what if not found?
		    // add code for Task 4 here


			System.out.print("* Enter name (or exit to quit): ");
			key = keyboard.next();
		}

	}
	
	
	// a utility method that gets a Scanner from a file-name
	private static Scanner getScanner(String fileName) {
		Scanner scnr = null;
		try {
			scnr = new Scanner ( new File(fileName) );
		} catch (FileNotFoundException e) {
			System.out.println("* Error: cannot open file getScanner(): " + fileName);
		}
		return scnr;
	}

}




























/*

// here's a working version of main()

public static void main(String args[]) {
    // two Collection objects for us to use
    TreeSet<String> allWords = new TreeSet<String>();
    TreeMap<String,String> addressMap = new TreeMap<String,String>();
    
    Scanner fileScanner = getScanner("name-address.txt");
    while ( fileScanner.hasNext() ) {
        // read lines two-at-a-time from a file
        String name = fileScanner.nextLine().trim();
        String address = fileScanner.nextLine().trim();
        System.out.println(name + ":" + address);
        
        // The TAs will demo something right here. 
        
        // Task 1:  store each person's name in the set
        allWords.add(name);
        
        // Task 2:  store each words in the address into the same set
        String[] wordsInLine = address.split("\\s+"); // split around white-space characters
        for (int i=0; i < wordsInLine.length; ++i)
            allWords.add(wordsInLine[i]);
        
        // Task 3: store each (name,address) pair into the map
        addressMap.put(name, address);
    }

    System.out.println("** allWords contains:\n" + allWords + "\n");
    System.out.println("** addressMap contains:\n" + addressMap + "\n");
    
    // Task 4: read key and look it up in map
    System.out.println("\n** Let's lookup names in addressMap.");
    Scanner keyboard = new Scanner(System.in);
    System.out.print("* Enter name (or exit to quit): ");
    String key = keyboard.next();
    while ( ! key.equals("exit") ) {
        String address = addressMap.get(key);
        System.out.println(key + " has address: " + address);
        System.out.print("* Enter name (or exit to quit): ");
        key = keyboard.next();
    }

}



*/



