/*
 * CS2110: Lab on Maps
 * This program demos how to do two tasks with Maps.  Both tasks involve reading words from
 * a file (part of "Green Eggs and Ham") and processing them.
 * 
 * (1) The first task was demo'd in lecture.  You want to count how often each word occurs.
 * You need a TreeMap that maps Strings to Integers.
 * Your job in lab: repeat what we showed you in lecture to make sure you understand how it
 * was done.  See the comment that says "Task 1" below for instructions and hints.
 * 
 * (2) The second task is similar but a bit more complex.  You want to store which lines each word
 * was found on in the file.
 * You need a TreeMap that maps String to a list of line-numbers (Integers).  So here the value stored
 * for each key is list, not just a single value like one Integer as it was in Task 1.
 * The key here is that you must create a new List and put it into the TreeMap the very first time
 * you see a word. Then later, when you see that word again, you retrieve the List and
 * add a new value to that list.
 * See the comment below that says "Task 2" for instructions and hints.
 * 
 */ 

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class MapExercise {
	
	public static void main(String args[]) {
		MapExercise theApp = new MapExercise();
		
		theApp.findUniqueWords("seuss1.txt");
		
		// find words and how often they occur.
		//    need a map from word-string to an integer-count
		TreeMap<String,Integer> seussIndex = new TreeMap<String,Integer>();
				theApp.makeIndex("seuss1.txt", seussIndex);
		theApp.printIndex(seussIndex, "Word Counts from Dr. Seuss");
		
		// find words and the line numbers they occur on.
		//    need a map from word-string to a list of line-numbers
		TreeMap<String,ArrayList<Integer>> seussLineIndex = new TreeMap<String,ArrayList<Integer>>();
		theApp.makeLineIndex("seuss1.txt", seussLineIndex);				
		theApp.printLineIndex(seussLineIndex, "Words By Line From Dr. Seuss");
		
	}

	
	// a simple function that finds and prints the unique words found in a file
    private void findUniqueWords(String fileName) {
        Scanner scnr = getScanner(fileName);
        if (scnr == null ) {
            System.out.println("* Error: cannot open file: " + fileName);
            return;
        }
        
        // Task 0 starts here: declare a TreeSet to hold words.
        
        while ( scnr.hasNext() ) {
            String word = scnr.next();
            /*
             * More for Task 0 here:  you've got a word.  Add it to the Set in a way
             * that no duplicates are stored.
             * Add lines of code here.  You need only a very small number of lines here!
             */
            

        }
        // More for Task 0 here:
        // Add code here to print out your Set here. See if the tonString() for TreeSet
        // does something nice when you print out the Set
        
        
    }
    
	
	// Make a map that stores how often words occur in a file.
	// The 2nd parameter must be an existing Map, perhaps empty.
	// When the method returns, this map will be updated to contain key-value
	// pairs for each word and its frequency in the file.
	private boolean makeIndex(String fileName, TreeMap<String,Integer> wordIndex) {
		Scanner scnr = getScanner(fileName);
		if (scnr == null ) return false;
		
		while ( scnr.hasNext() ) {
			String word = scnr.next();
			/*
			 * Task 1 here:  you've got a word.  Now, if it's a key in the map
			 * already, add one to the count that's already stored in the map.
			 * If it's not in the map, add it to the map with a count of 1.
			 * Methods you might need: containsKey(), get(), put()
			 * Add lines of code here.  You need only 4-8 lines or so of code here.
			 */

		} 		
		return true;		
	}

	// a utility method that gets a Scanner from a file-name
	private static Scanner getScanner(String fileName) {
		Scanner scnr = null;
		try {
			scnr = new Scanner ( new File(fileName) );
		} catch (FileNotFoundException e) {
			System.out.println("* Error: cannot open file in MapDemo: " + fileName);
		}
		return scnr;
	}

	// print out the word-count map
	private void printIndex(TreeMap<String, Integer> wordIndex, String header) {
			System.out.println("** " + header + " **");
			Set<String> words = wordIndex.keySet();
			for (String word : words ) {
				System.out.println("  " + word + " " + wordIndex.get(word));
			}
	/*
			// what would this code do if it replaced the code above?
			ArrayList<String> wordList = new ArrayList<String>(words);
			Collections.sort(wordList);
			for (String word : wordList ) {
				System.out.println("  " + word + " " + wordIndex.get(word));
			}
	*/
		}

	// Make a TreeMap that stores what lines each word is found on in a file.
	// The 2nd parameter must be an existing Map, perhaps empty.
    // When the method returns, this map will be updated to contain key-value
    // pairs for each word and a list of the line numbers where it's found in the file.
	private boolean makeLineIndex(String fileName,
			TreeMap<String, ArrayList<Integer>> wordLineIndex) {
	
		Scanner scnr = getScanner(fileName);
		if (scnr == null ) return false;
		ArrayList<Integer> wordsLines = null; // declare reference to point to one list in the TreeMap
	
		// loop to process file line-by-line, keeping a line-counter
		int lineCtr = 0;
		while ( scnr.hasNext() ) {
			String line = scnr.nextLine();
			++lineCtr;
			line.trim(); // remove leading and trailing spaces
			String[] words = line.split("\\s+"); // split around white-space characters
			// at this point, array words contains each word found on the current line
	
			// loop over each word found in the current line (stored in array words)
			for (int i=0; i < words.length; ++i ) {
				String currentWord = words[i];
				// System.out.println("currentWord=>" + currentWord + "<"); // debugging line

				// Use reference to point to one list in the Map
				wordsLines = null; // assume word not in map
				/*
				 * Task 2 here:  Between this point and the line given below, you 
				 * need to use the ArrayList wordsLines (the lines that this word occurs in)
				 * as you do the following:
				 * (a) see if there's already a list stored in the map for the current-word;
				 * (b) if not, create one, put it in the map, and make sure wordsLines refers to (points to) it;
				 * (c) if it's there, retrieve it and make sure wordsLines refers to it.
				 */

				// add your code for Task 2 here!
				
				// at this point wordsLines points to a list in the map for the lines this word occurs in
				//wordsLines.add(lineCtr); // add the line-number for this occurrence to the list for currentWord
			}
		} 		
		return true;		
	}

	// prints the map that stores what line-numbers each word is found on
	private void printLineIndex(TreeMap<String, ArrayList<Integer>> wordLineIndex,
			String header) {
		System.out.println("** " + header + " **");
		Set<String> words = wordLineIndex.keySet(); // get all keys in the Map
		for (String word : words ) {
			System.out.println("  " + word + " " + wordLineIndex.get(word));
		}		
	}

}
